PROJECT = resolvers_yang
VERSION = 0.1
.PHONY = tags deps install-deps

tags:
	find $(PROJECT) -name "*.py" | etags -

deps:
	mv requirements.txt requirements.txt.old
	pip freeze > requirements.txt

install-deps:
	pip install -r requirements.txt


