# Resolvers-YANG

YANG data models and tools for unified configuration of DNS resolvers

* [Official Documentation](https://labs.pages.labs.nic.cz/resolvers-yang/)
* [Examples of usage](https://gitlab.labs.nic.cz/labs/resolvers-yang/wikis/examples)
* [Wiki](https://gitlab.labs.nic.cz/labs/resolvers-yang/wikis/home)
* [Deckard config](https://gitlab.labs.nic.cz/labs/resolvers-yang/wikis/deckard)

Data model and library for DNS resolvers:
* [Knot Resolver](https://www.knot-resolver.cz/)
* [Unbound](https://www.unbound.net/)

## Data Model
* [Current schema tree](https://gitlab.labs.nic.cz/labs/resolvers-yang/raw/master/yang-modules/model.tree)
* [Example JSON data](https://gitlab.labs.nic.cz/labs/resolvers-yang/raw/master/examples/example-data.json)

#### YANG Modules
* [cznic-resolver-common](https://gitlab.labs.nic.cz/labs/resolvers-yang/raw/master/yang-modules/cznic-resolver-common.yang)
* [cznic-resolver-knot](https://gitlab.labs.nic.cz/labs/resolvers-yang/raw/master/yang-modules/cznic-resolver-knot.yang)
* [cznic-resolver-unbound](https://gitlab.labs.nic.cz/labs/resolvers-yang/raw/master/yang-modules/cznic-resolver-unbound.yang)
* [cznic-dns-rdata](https://gitlab.labs.nic.cz/labs/resolvers-yang/raw/master/yang-modules/cznic-dns-rdata.yang)
* [cznic-deckard](https://gitlab.labs.nic.cz/labs/resolvers-yang/raw/master/yang-modules/cznic-deckard.yang)


## Getting Started

Requires **Python 3.6** or newer

```bash
# Ubuntu/Debian
$ sudo apt-get install python3 python3-pip

# Arch Linux
$ sudo pacman -S python python-pip
```

**Installation**

The Development instructions are [here](https://gitlab.labs.nic.cz/labs/resolvers-yang/wikis/devinstall).

```bash
$ git clone https://gitlab.labs.nic.cz/labs/resolvers-yang.git
$ cd resolvers-yang
$ python3 setup.py install
```


## Create local documentation

Local [Sphinx](http://www.sphinx-doc.org/en/master/) documentation is located in `docs` directory.

```
$ cd docs/
$ make html
```

Then open `_build/html/index.html` in your web browser.