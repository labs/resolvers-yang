.. _examples:

*********
Examples
*********

All examples are located in ``examples/`` directory.

**Requirements:**

* Python 3.6+
* `resolvers-yang <https://gitlab.labs.nic.cz/labs/resolvers-yang>`_
* `yangson <https://github.com/CZ-NIC/yangson>`_

Using Python virtual environment is recommended. Look at :ref:`installation`

=================
example-data.json
=================

``example-data.json`` is example of Json-encoded file, which is valid against `resolvers-yang` data model

You can edit this Json and validate it against data model using::

    $ make validate

No output means that ``example-data.json`` is valid.

================
generate_conf.py
================

The script will generate  ``unbound.conf`` and ``kresd.conf`` resolvers configuration files.

Run ``generate_conf.py`` script with path to Json file as parameter::

    $ python generate_conf.py example-data.json

If generated ``unbound.conf`` is converted to JSON by ``load_unb_conf_file_to_json.py`` script. The result does not have to be the same as input JSON for ``generate_conf.py`` script, because the configuration exclusively for Knot Resolver is ignored for Unbound.

=============================
unbound_to_json.py
=============================

The script will convert Unbound configuration file ``unbound.conf`` to Json-encoded file ``unb-data.json``, which is validate with ``resolvers-yang`` data model.
As example configuration file can be used ``unbound.conf`` created by running ``generate_conf.py``

Run ``unbound_to_json.py`` with path to ``unbound.conf`` as parameter::

    $ python unbound_to_json.py unbound.conf

No output means that the data in created ``unb-data.json`` are valid.

If you generate another ``unbound.conf`` from this Json using ``generate_conf.py`` script, it should be equal to ``unbound.conf``, which was used as input parameter for ``load_unb_conf_file_to_json.py`` script.