.. Resolvers-YANG documentation master file, created by
   sphinx-quickstart on Tue Sep  4 15:42:13 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Resolvers-YANG
==========================================
YANG data models, python3 library and tools for unified configuration of DNS resolvers.

* `Knot Resolver <https://www.knot-resolver.cz/>`_
* `Unbound <https://www.unbound.net/>`_


.. toctree::
   :maxdepth: 2

   installation
   library
   model
   examples
   references


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`


