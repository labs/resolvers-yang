.. _installation:

**************************
Getting Started
**************************

Resolvers-YANG library requires `Python 3.6 <https://www.python.org/>`_ or newer::

    # Ubuntu/Debian
    $ sudo apt-get install python3 python3-pip

    # Arch Linux
    $ sudo pacman -S python python-pip

===========
Development
===========

1. Clone the project in a directory of your choice::

    $ git clone git@gitlab.labs.nic.cz:labs/resolvers-yang.git

2. Create the virtual environment::

    $ python3 -m venv resolvers-yang

3. Activate the virtual environment::

        $ cd resolvers-yang
        $ source bin/activate

When inside the virtual environment, the shell prompt should change to something like::

    (resolvers-yang) $

4. Install required standard packages inside the virtual environment::

    $ pip install -r requirements.txt

If you are prompted to upgrade *pip*, you can do that, too.

5. To leave the virtual environment, just do::

    (resolvers-yang) $ deactivate

The virtual environment can be entered anytime later by executing step 3. The steps preceding it need to be performed just once.

===================
System Installation
===================
::

    $ git clone https://gitlab.labs.nic.cz/labs/resolvers-yang
    $ cd resolvers-yang
    $ python3 setup.py install
