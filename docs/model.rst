***************
YANG Data Model
***************

------------------------------------------------------------

=============
YANG Modules
=============

All Modules are located in ``yang-modules/`` project directory.

* `cznic-dns-rdata`_
* `cznic-resolver-common`_
* `cznic-resolver-knot`_


------------------------------------------------------------

=============
Data Model
=============

* `Current schema tree`_
* `Example JSON data`_

----------------------------------------
Common Model - `cznic-resolver-common`_
----------------------------------------
Common Data model configuration options are divided into several sections.

In order to unify Knot and Unbound configuration into the `cznic-resolver-common`_,
some configurations are not generated directly from value in the common model, but before generating
must be converted to configuration value which is accepted by resolver.

Here is how some values are converted:

server:
^^^^^^^

There is no ``group-name`` option for Unbound to set.

.. code-block:: python

    # DataModel:
    "server": {
        "user-name": "username",
        "group-name": "group"
    }

    # KnotResolver:
    user('username','group')        # Conversion:  no conversion

    # Unbound:
    username: "username"            # Conversion:  no conversion


network:
^^^^^^^^

resolver:
^^^^^^^^^
**stub-zones:**

.. code-block:: python

    # DataModel:
    "stub-zones": [
        {
        "domain": "stub.example.com",
        "nameserver": "192.0.2.1",
        "port": 53                             # Default:  53
        }
    ]

    # KnotResolver:
    STUB('192.0.2.1@53')                       # Conversion:  'nameserver' + port -> nameserver@port'

    # Unbound:
    stub-zone:
       name: "stub.example.com"                # Conversion:   no conversion
       stub-addr: "192.0.2.1@53"               # Conversion:  'nameserver' + port -> nameserver@port'

Knot Resolver ``STUB(ip)`` configuration only accepts IP addresses. In common configuration option
``"nameserver"`` can be Name or IP address. If ``"nameserver"`` option is Name, ``gethostbyname`` method from
module ``socket`` is used to ask for IP address of this Name. For Unbound is used ``stub-host:`` configuration function insted of ``stub-addr:``

**options:**

.. code-block:: python

    # DataModel:
    "options": {
        "glue-checking": true | false,
    }

    # KnotResolver:
    mode('strict' | 'normal' | 'permissive')    # Conversion:   true -> "strict"
                                                                false -> "normal"

    # Unbound:
    harden-glue: yes | no                       # Conversion:   no conversion


Knot Resolver has three options of ``glue-checking``, but to simplify the data model, there is conversion to two options like it is in Unbound.


logging
^^^^^^^

**verbosity:**

.. code-block:: python

    # DataModel:
    "logging": {
      "verbosity": 0-5       # Default:  1
    }

    # KnotResolver:
    verbosity: 0-5           # Conversion:   no conversion

    # Unbound:
    verbose(true | false)    # Conversion:   0 -> false
                             #               1-5 -> true


dnssec:
^^^^^^^

cache:
^^^^^^

dns64:
^^^^^^


.. _Current schema tree: https://gitlab.labs.nic.cz/labs/resolvers-yang/raw/master/yang-modules/model.tree
.. _Example JSON data: https://gitlab.labs.nic.cz/labs/resolvers-yang/raw/master/examples/example-data.json
.. _cznic-dns-rdata: https://gitlab.labs.nic.cz/labs/resolvers-yang/raw/master/yang-modules/cznic-dns-rdata.yang
.. _cznic-resolver-common: https://gitlab.labs.nic.cz/labs/resolvers-yang/raw/master/yang-modules/cznic-resolver-common.yang
.. _cznic-resolver-knot: https://gitlab.labs.nic.cz/labs/resolvers-yang/raw/master/yang-modules/cznic-resolver-knot.yang
