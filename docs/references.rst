**********
References
**********

.. [RFC7895] Bierman, A.; Bjorklund, M.; Watsen, K. *YANG Module
	     Library.* `RFC 7895`__, IETF, 2016. 13 p. ISSN 2070-1721.

__ https://tools.ietf.org/html/rfc7895


.. [RFC7951] Lhotka, L. *JSON Encoding of Data Modeled with YANG.*
	   `RFC 7951`__, IETF, August 2016. 20 p. ISSN 2070-1721.

__ https://tools.ietf.org/html/rfc7951
