import sys
from json import load, dumps
from yangson.datamodel import DataModel

from resolvers_yang.unbound import unbound_conf_gen
from resolvers_yang.kresd import kresd_conf_gen

json_path = sys.argv[1]

# set file names and paths
yangdir = "../../yang-modules"
kresd_path = "kresd.conf"
unb_path = "unbound.rpl"

# load model
model = DataModel.from_file(yangdir + "/yanglib-deckard.json", [yangdir])

# load data from json
with open(json_path) as infile:
    ri = load(infile)

data = model.from_raw(ri)

# validate against data model
data.validate()

# adding missing default values
data = data.add_defaults()

data.validate()

# get path where is mock data located
if 'cznic-deckard:deckard' in data:
    mock_path = data["cznic-deckard:deckard"]["mock-data"].value

    # load mock data text file from path as string
    mock_data = open(mock_path).read()

    # slicing mock_data
    mock_begin = mock_data.find("SCENARIO_BEGIN")
    mock_data = "\nCONFIG_END\n\n" + mock_data[mock_begin:]
else:
    mock_data = ""
    print("Failed to load mock-data: 'cznic-deckard:deckard' node not found ")

# generate configuration strings
unbound_conf = unbound_conf_gen.generate(data)
kresd_conf = kresd_conf_gen.generate(data)

# write Knot Resolver configuration
with open(kresd_path, "w") as knot_file:
    knot_file.write(kresd_conf)

# write unbound.rpl = unbconf string + mock data
with open(unb_path, "w") as unb_file:
    unb_file.write(unbound_conf + mock_data)

