import sys
from json import load
from yangson.datamodel import DataModel

from resolvers_yang.unbound import unbound_conf_gen
from resolvers_yang.kresd import kresd_conf_gen

json_path = sys.argv[1]

# path to directory where are yang modules
yangdir = "../yang-modules"

# names of configuration files and paths where it will be created
kresd_path = "kresd.conf"
unb_path = "unbound.conf"

# yangson load DataModel
model = DataModel.from_file(yangdir + "/yanglib.json", [yangdir])

# load data from Json-encoded file
with open(json_path) as infile:
    ri = load(infile)

# load data to DataModel
data = model.from_raw(ri)

# validate against DataModel
data.validate()

# adding missing default values
data_defaults = data.add_defaults()

# generate configuration strings
unbound_conf = unbound_conf_gen.generate(data_defaults)
kresd_conf = kresd_conf_gen.generate(data_defaults)

# write Knot Resolver configuration
with open(kresd_path, "w") as knot_file:
    knot_file.write(kresd_conf)

# write Unbound configuration
with open(unb_path, "w") as unb_file:
    unb_file.write(unbound_conf)


