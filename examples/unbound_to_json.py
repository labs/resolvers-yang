import sys
from json import dump

from yangson.datamodel import DataModel
from resolvers_yang.unbound import unbound_conf_loader

unbconf_path = sys.argv[1]

# path to directory where are yang modules
yangdir = "../yang-modules"

# name of json file and path where it will be created
data_json_path = "unb-data.json"

# yangson load DataModel
model = DataModel.from_file(yangdir + "/yanglib.json", [yangdir])

# load unbound.conf file
with open(unbconf_path, "r") as unb_file:
    unbconf_data = unb_file.read()

# call of conversion function     unbconf_data >> dictionary
json_data = unbound_conf_loader.load(unbconf_data)

# load data to DataModel
model_data = model.from_raw(json_data)

# validate data against DataModel
model_data.validate()

# save model_data to json
with open(data_json_path, 'w') as json_file:
    dump(json_data, json_file, indent=2, sort_keys=False)

