"""
Module for converting data from valid loaded Json(Python Dictionary) to Knot Resolver configuration strings.
"""

from re import sub
from resolvers_yang.parser import TrustAnchorRR as TA_parser
from resolvers_yang.regex import ipv6_prefix


def generate(config: dict) -> str:
    """
    :param config: Dictionary loaded from Resolvers-YANG Model valid Json file.
    :type config: dict
    :return: Configuration strings for Knot-Resolver separated by newlines.
    :rtype: str
    """
    kresd_conf = ""
    if 'cznic-resolver-common:dns-resolver' in config:
        conf = config['cznic-resolver-common:dns-resolver']
    else:
        conf = config

    if 'network' in conf:
        if kresd_conf != "":
            kresd_conf += "\n"
        kresd_conf += Network.generate(conf['network'])
    if 'server' in conf:
        if kresd_conf != "":
            kresd_conf += "\n"
        kresd_conf += Server.generate(conf['server'])
    if 'resolver' in conf:
        if kresd_conf != "":
            kresd_conf += "\n"
        kresd_conf += Resolver.generate(conf['resolver'])
    if 'logging' in conf:
        if kresd_conf != "":
            kresd_conf += "\n"
        kresd_conf += Logging.generate(conf['logging'])
    if 'dnssec' in conf:
        if kresd_conf != "":
            kresd_conf += "\n"
        kresd_conf += Dnssec.generate(conf['dnssec'])
    if 'cache' in conf:
        if kresd_conf != "":
            kresd_conf += "\n"
        kresd_conf += Cache.generate(conf['cache'])
    if 'dns64' in conf:
        if kresd_conf != "":
            kresd_conf += "\n"
        kresd_conf += Dns64.generate(conf['dns64'])

    return kresd_conf


class Server:

    @staticmethod
    def generate(server: dict) -> str:
        server_conf = ""
        server_conf += Server.username(server['user-name'], server['group-name'])

        return server_conf

    @staticmethod
    def username(username: str, groupname: str=None) -> str:

        if groupname is None or groupname is "":
            conf_string = "user('{0}')".format(username)
        else:
            conf_string = "user('{0}','{1}')".format(username, groupname)

        return conf_string


class Network:

    @staticmethod
    def generate(network: dict) -> str:
        network_conf = ""
        if 'listen-interfaces' in network:
            if network_conf != "":
                network_conf += "\n"
            network_conf += Network.listen_interfaces(network['listen-interfaces'])
        if 'source-address' in network:
            if network_conf != "":
                network_conf += "\n"
            network_conf += Network.source_address(network['source-address'])
        if 'recursion-transport' in network:
            if network_conf != "":
                network_conf += "\n"
            network_conf += Network.recursion_transport(network['recursion-transport'])
        if 'udp-payload-size' in network:
            if network_conf != "":
                network_conf += "\n"
            network_conf += Network.udp_payload_size(network['udp-payload-size'])

        return network_conf

    @staticmethod
    def listen_interfaces(interfaces: list) -> str:
        # TODO: not sure if this is right
        intrfc = []
        conf_string = ""

        for interface in interfaces:
            temp = ""
            # name
            if 'name' in interface:
                pass
            # ip-address
            if 'ip-address' in interface:
                temp = str(interface["ip-address"])
            # port
            if 'port' in interface:
                temp += "@" + str(interface["port"])

            intrfc.append(temp)

        if intrfc:
            conf_string = str("net = {" + str(intrfc)[1:-1] + "}")

        return conf_string

    @staticmethod
    def source_address(source_address: dict) -> str:

        if 'ipv4' in source_address and 'ipv6' in source_address:
            sa_v4 = source_address['ipv4'].value
            sa_v6 = source_address['ipv6'].value
            conf_string = "net.outgoing_v4('{0}')\nnet.outgoing_v6('{1}')".format(sa_v4, sa_v6)
        elif 'ipv4' in source_address:
            sa_v4 = source_address['ipv4'].value
            conf_string = "net.outgoing_v4('{0}')".format(sa_v4)
        elif 'ipv6' in source_address:
            sa_v6 = source_address['ipv6'].value
            conf_string = "net.outgoing_v6('{0}')".format(sa_v6)
        else:
            conf_string = ""

        return conf_string

    @staticmethod
    def client_transport() -> str:
        pass

    @staticmethod
    def recursion_transport(recursion_transport: dict) -> str:
        """Converting 'recursion-transport' data model option to Knot-Resolver configuration string.

                Conversion to Kresd:
                    'ipv4 ipv6'   ->   'net.ipv6 = true'
                                       'net.ipv4 = true'
                    'ipv4'        ->   'net.ipv6 = false'
                                       'net.ipv4 = true'
                    'ipv6'        ->   'net.ipv6 = true'
                                       'net.ipv4 = false'
                """
        conf_string = ""

        if 'l2-protocols' in recursion_transport:
            protocols = str(recursion_transport['l2-protocols'])

            if 'ipv4' in protocols and 'ipv6' in protocols:
                conf_string = "net.ipv6 = true\nnet.ipv4 = true"
            elif 'ipv4' in protocols:
                conf_string = "net.ipv6 = false\nnet.ipv4 = true"
            elif 'ipv6' in protocols:
                conf_string = "net.ipv6 = true\nnet.ipv4 = false"
            else:
                conf_string = "net.ipv6 = false\nnet.ipv4 = false"
                # raise Exception?

        return conf_string

    @staticmethod
    def udp_payload_size(payload_size: int):
        conf_string = "net.bufsize({0})".format(payload_size)
        return conf_string


class Resolver:

    @staticmethod
    def generate(resolver: dict) -> str:
        resolver_conf = ""
        if 'stub-zones' in resolver:
            if resolver_conf != "":
                resolver_conf += "\n"
            resolver_conf += Resolver.stub_zones(resolver['stub-zones'])
        # hints
        if 'hints' in resolver:
            hints = resolver['hints']
            if 'cznic-resolver-knot:hint' in hints:
                if resolver_conf != "":
                    resolver_conf += "\n"
                resolver_conf += Resolver.hints_hint(hints['cznic-resolver-knot:hint'])
            if 'cznic-resolver-knot:hosts-file' in hints:
                if resolver_conf != "":
                    resolver_conf += "\n"
                resolver_conf += Resolver.hints_hosts_file(hints['cznic-resolver-knot:hosts-file'])
            if 'root-hint' in hints:
                if resolver_conf != "":
                    resolver_conf += "\n"
                resolver_conf += Resolver.hints_root_hint(hints['root-hint'])
            if 'root-zone-file' in hints:
                if resolver_conf != "":
                    resolver_conf += "\n"
                resolver_conf += Resolver.hints_root_zone_file(hints['root-zone-file'])
        # options
        if 'options' in resolver:
            options = resolver['options']
            if 'glue-checking' in options:
                if resolver_conf != "":
                    resolver_conf += "\n"
                resolver_conf += Resolver.option_glue_checking(options['glue-checking'])
            if 'qname-minimisation' in options:
                if resolver_conf != "":
                    resolver_conf += "\n"
                resolver_conf += Resolver.option_qname_minimisation(options['qname-minimisation'])
            if 'reorder-rrset' in options:
                if resolver_conf != "":
                    resolver_conf += "\n"
                resolver_conf += Resolver.option_reorder_rrset(options['reorder-rrset'])
            if 'query-loopback' in options:
                if resolver_conf != "":
                    resolver_conf += "\n"
                resolver_conf += Resolver.option_query_loopback(options['query-loopback'])

        return resolver_conf

    @staticmethod
    def stub_zones(zones: list) -> str:
        # TODO: Unbound can resolve host name Kresd can not.
        #conf_strings = [str]
        conf_string = ""
        for zone in zones:

            # if ipv4_address.match(str(zone["nameserver"])) or ipv6_address.match(str(zone["nameserver"])):
            #     nameserver = zone['nameserver']
            # else:
            #     nameserver = gethostbyname(str(zone["nameserver"]))

            if 'port' in zone:
                address = "{0}@{1}".format(zone["nameserver"], str(zone['port']))
            else:
                address = zone["nameserver"]

            if conf_string is not "":
                conf_string += "\n"
            #conf_strings.append("policy.add(policy.suffix(policy.STUB('{0}'), {policy.todname('{1}')}))".format(address, zone['domain']))
            conf_string += "policy.add(policy.suffix(policy.STUB('"+address+"'), " \
                           "{policy.todname('"+str(zone['domain'])+"')}))"

        return conf_string

    @staticmethod
    def hints_hint(hints: list) -> str:
        conf_string = "modules.load('hints')"
        canonical_hints = ""

        for hint in hints:
            for value in hint['values']:

                if hint['canonical'] is True:
                    if canonical_hints is not "":
                        canonical_hints += "\n"
                    canonical_hints += "hints.set(\"{0} {1}\")".format(hint['name'], value)
                else:
                    if conf_string is not "":
                        conf_string += "\n"
                    conf_string += "hints.set(\"{0} {1}\")".format(hint['name'], value)

        conf_string += canonical_hints

        return conf_string

    @staticmethod
    def hints_hosts_file(hf_path: str) -> str:
        conf_string = "hints.add_hosts('{0}')".format(hf_path)

        return conf_string

    @staticmethod
    def hints_root_hint(root_hints: list) -> str:
        root_hints_list = []

        for rh in root_hints:
            r_hint = "['" + rh['name'].value + "'] = {" + str(rh['values'])[1:-1] + "}"
            root_hints_list.append(r_hint)

        conf_string = "hints.root({" + str(", ".join(root_hints_list)) + "})"

        return conf_string

    @staticmethod
    def hints_root_zone_file(rzf_path: str) -> str:
        conf_string = "hints.root_file('{0}')".format(rzf_path)

        return conf_string

    @staticmethod
    def option_glue_checking(checking: bool) -> str:
        """Converting 'glue-checking' data model option to Knot-Resolver configuration string.

        :param checking: Glue Checking on/off.
        :type checking: bool
        :returns:  str
        :raises: AttributeError, KeyError

        Conversion to Kresd:
            true    ->  'mode('strict')'
            false   ->  'mode('normal')'
        """

        if bool(checking) is True:
            conf_string = "mode('strict')"
        elif bool(checking) is False:
            conf_string = "mode('normal')"
        else:
            conf_string = ""

        return conf_string

    @staticmethod
    def option_qname_minimisation(minimisation: bool) -> str:
        """Converting resolvers-yang data model 'qname-minimisaton' option to Knot-Resolver configuration string.

        :param minimisation: Qname Minimisation on/off.
        :type minimisation: bool
        :returns:  str
        :raises: AttributeError, KeyError

        Conversion to Kresd:
            true    ->  'option('NO_MINIMIZE', false)'
            false   ->  'option('NO_MINIMIZE', true)'
        """

        if bool(minimisation) is True:
            conf_string = "option('NO_MINIMIZE', false)"
        elif bool(minimisation) is False:
            conf_string = "option('NO_MINIMIZE', true)"
        else:
            conf_string = ""

        return conf_string

    @staticmethod
    def option_query_loopback(q_loopback: bool) -> str:
        """Converting resolvers-yang data model 'query-loopback' option to Knot-Resolver configuration string.

        :param q_loopback: Glue Checking on/off.
        :type q_loopback: bool
        :returns:  str
        :raises:

        Conversion to Kresd:
            true    ->  'option('ALLOW_LOCAL', true)'
            false   ->  'option('ALLOW_LOCAL', false)'
        """

        if bool(q_loopback) is True:
            conf_string = "option('ALLOW_LOCAL', true)"
        elif bool(q_loopback) is False:
            conf_string = "option('ALLOW_LOCAL', false)"
        else:
            conf_string = ""
            # raise Error?

        return conf_string

    @staticmethod
    def option_reorder_rrset(reorder: bool) -> str:
        """Converting resolvers-yang data model 'reorder-rrset' option to Knot-Resolver configuration string.

        :param reorder: Reorder rrset.
        :type reorder: bool
        :returns:  str
        :raises:

        Conversion to Kresd:
            true    ->  'reorder_RR(true)'
            false   ->  'reorder_RR(false)'
        """

        if bool(reorder) is True:
            conf_string = "reorder_RR(true)"
        elif bool(reorder) is False:
            conf_string = "reorder_RR(false)"
        else:
            conf_string = ""

        return conf_string


class Logging:

    @staticmethod
    def generate(logging: dict) -> str:
        logging_conf = ""
        if 'verbosity' in logging:
            logging_conf += Logging.verbosity(logging['verbosity'].value)

        return logging_conf

    @staticmethod
    def verbosity(verbosity: int) -> str:
        """Converting resolvers-yang model 'verbosity' to Knot-Resolver configuration string.

        :param verbosity: Verbosity level, range 0-5.
        :type verbosity: int
        :returns:  str
        :raises:

        Conversion to Kresd:
            0   ->  'verbose(false)'
            1-5 ->  'verbose(true)'
        """
        if verbosity == 0:
            conf_string = "verbose(false)"
        elif verbosity <= 5:
            conf_string = "verbose(true)"
        else:
            conf_string = ""

        return conf_string


class Dnssec:

    @staticmethod
    def generate(dnssec: dict) -> str:
        dnssec_conf = ""
        if 'trust-anchors' in dnssec:
            if dnssec_conf != "":
                dnssec_conf += "\n"
            dnssec_conf += Dnssec.trust_anchors(dnssec['trust-anchors'].value)
        if 'negative-trust-anchors' in dnssec:
            if dnssec_conf != "":
                dnssec_conf += "\n"
            dnssec_conf += Dnssec.negative_trust_anchors(dnssec['negative-trust-anchors'].value)

        return dnssec_conf

    @staticmethod
    def trust_anchors(trust_anchors: list):
        conf_string = ""
        for trust_anchor in trust_anchors:

            if trust_anchor['auto-update']:
                break

            if 'trust-anchor' in trust_anchor:
                for ta in trust_anchor['trust-anchor']:
                    if 'ds' in ta:
                        ta_string = TA_parser.create_ds_string(trust_anchor['domain'].value,
                                                               ta['ds'].value)
                    elif 'dnskey' in ta:
                        ta_string = TA_parser.create_dnskey_string(trust_anchor['domain'].value,
                                                                   ta['dnskey'])
                    else:
                        break

                    if conf_string != "":
                        conf_string += "\n"
                    conf_string += "trust_anchors.add('{0}')".format(ta_string)

        return conf_string

    @staticmethod
    def negative_trust_anchors(negative_ta: list):
        conf_string = "trust_anchors.negative = {" + str(negative_ta)[1:-1] + "}"
        return conf_string


class Cache:

    @staticmethod
    def generate(cache: dict) -> str:
        cache_conf = ""
        if 'max-size' in cache:
            if cache_conf != "":
                cache_conf += "\n"
            cache_conf += Cache.max_size(cache['max-size'])
        if 'max-ttl' in cache:
            if cache_conf != "":
                cache_conf += "\n"
            cache_conf += Cache.max_ttl(cache['max-ttl'])
        if 'min-ttl' in cache:
            if cache_conf != "":
                cache_conf += "\n"
            cache_conf += Cache.min_ttl(cache['min-ttl'])
        if 'cznic-resolver-knot:prefill' in cache:
            if cache_conf != "":
                cache_conf += "\n"
            cache_conf += Cache.prefill(cache['cznic-resolver-knot:prefill'])

        return cache_conf

    @staticmethod
    def current_size():
        conf_string = "cache.current_size"
        return conf_string

    @staticmethod
    def max_size(size: int) -> str:
        conf_string = "cache.open({0})".format(size)
        return conf_string

    @staticmethod
    def max_ttl(max_ttl: int) -> str:
        conf_string = "cache.max_ttl({0})".format(max_ttl)
        return conf_string

    @staticmethod
    def min_ttl(min_ttl: int) -> str:
        conf_string = "cache.min_ttl({0})".format(min_ttl)
        return conf_string

    @staticmethod
    def prefill(prefill: list) -> str:
        config = []

        for item in prefill:
            prefill_str = "{['" + item['origin'].value + "'] = {url = '" + item['url'].value + "', " \
                          "ca_file = '" + item['ca-file'].value + "', interval = 86400}}"
            config.append(prefill_str)

        conf_string = "modules.load('prefill')\nprefill.config(" + str(", ".join(config)) + ")"

        return conf_string


class Debugging:
    pass


class Dns64:

    @staticmethod
    def generate(dns64: dict):
        dns64_conf = ""
        if 'prefix' in dns64:
            if dns64_conf != "":
                dns64_conf += "\n"
            dns64_conf += Dns64.prefix(dns64['prefix'])

        return dns64_conf

    @staticmethod
    def prefix(prefix: str) -> str:
        prefix = sub(ipv6_prefix, '', str(prefix))
        conf_string = "modules = {dns64 = '" + str(prefix) + "'}"

        return conf_string
