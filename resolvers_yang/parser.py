from .regex import ipv4_address, ipv6_address


##########################################################################
# Trust-Anchor ResourceRecord PARSER
##########################################################################
class TrustAnchorRR:

    alg_enum = {
        1: "RSAMD5",
        2: "DH",
        3: "DSA",
        5: "RSASHA1",
        6: "DSA-NSEC3-SHA1",
        7: "RSASHA1-NSEC3-SHA1",
        8: "RSASHA256",
        10: "RSASHA512",
        12: "ECC-GOST",
        13: "ECDSAP256SHA256",
        14: "ECDSAP384SHA384"
    }

    alg_enum_inv = {v: k for k, v in alg_enum.items()}

    digest_type_enum = {
        1: "SHA-1",
        2: "SHA-256",
        3: "GOST-R-34.11-94",
        4: "SHA-384"
    }

    digest_type_enum_inv = {v: k for k, v in digest_type_enum.items()}

    @staticmethod
    def parse_ds(ds_ta: str) -> dict:
        ds_ta.strip()

        owner = ds_ta.split()[0]

        i = 1
        if ds_ta.split()[i] != "IN" and ds_ta.split()[i] != "DS":
            i = i + 1
        if ds_ta.split()[i] == "IN":
            i = i + 1
        if ds_ta.split()[i] == "DS":
            i = i + 1

        ta_id = 0
        key_tag = ds_ta.split()[i]
        algorithm = TrustAnchorRR.alg_enum.get(int(ds_ta.split()[i + 1]))
        digest_type = TrustAnchorRR.digest_type_enum.get(int(ds_ta.split()[i + 2]))
        digest = ds_ta.split()[i + 3]

        ds_dict = {
            "owner": owner,
            "id": int(ta_id),
            "ds": {
                "algorithm": algorithm,
                "digest": digest,
                "digest-type": digest_type,
                "key-tag": int(key_tag)
            }
        }
        return ds_dict

    @staticmethod
    def parse_dnskey(dnskey_ta: str) -> dict:
        dnskey_ta.strip()

        owner = dnskey_ta.split()[0]

        i = 1
        if dnskey_ta.split()[i] != "IN" and dnskey_ta.split()[i] != "DNSKEY":
            i = i + 1
        if dnskey_ta.split()[i] == "IN":
            i = i + 1
        if dnskey_ta.split()[i] == "DNSKEY":
            i = i + 1

        ta_id = 0
        flags_num = int(dnskey_ta.split()[i])
        protocol = dnskey_ta.split()[i + 1]
        algorithm = TrustAnchorRR.alg_enum.get(int(dnskey_ta.split()[i + 2]))
        public_key = dnskey_ta.split()[i + 3]

        public_key = public_key.lstrip('(')
        public_key = public_key.rstrip(')')

        if flags_num == 257:
            flags = "ZONE SEP"
        elif flags_num == 256:
            flags = "ZONE"
        else:
            flags = False

        dnskey_dict = {
            "owner": owner,
            "id": int(ta_id),
            "dnskey": {
                "algorithm": algorithm,
                "flags": flags,
                "protocol": int(protocol.strip()),
                "public-key": public_key
            }
        }

        if not flags:
            del dnskey_dict['dnskey']['flags']

        return dnskey_dict

    @staticmethod
    def create_ds_string(domain: str, ds_ta: dict) -> str:

        algorithm = TrustAnchorRR.alg_enum_inv.get(str(ds_ta['algorithm']))
        digest_type = TrustAnchorRR.digest_type_enum_inv.get(str(ds_ta['digest-type']))

        ds_str = "{0} IN DS {1} {2} {3} {4}".format(domain,
                                                    ds_ta['key-tag'],
                                                    algorithm,
                                                    digest_type,
                                                    ds_ta['digest'])

        return ds_str

    @staticmethod
    def create_dnskey_string(domain: str, dnskey_ta: dict) -> str:

        algorithm = TrustAnchorRR.alg_enum_inv.get(str(dnskey_ta['algorithm']))
        flags = None

        if 'flags' in dnskey_ta:

            if 'SEP' in str(dnskey_ta['flags']) and 'ZONE' in str(dnskey_ta['flags']):
                flags = 257
            elif 'ZONE' in str(dnskey_ta['flags']) and 'SEP' not in str(dnskey_ta['flags']):
                flags = 256

        if flags is not None:
            dnskey_str = "{0} IN DNSKEY {1} {2} {3} {4}".format(domain,
                                                                flags,
                                                                dnskey_ta['protocol'], algorithm,
                                                                dnskey_ta['public-key'].decode('ascii'))
        else:
            dnskey_str = "{0} IN DNSKEY {1} {2} {3}".format(domain,
                                                            dnskey_ta['protocol'], algorithm,
                                                            dnskey_ta['public-key'].decode('ascii'))

        return dnskey_str


##########################################################################
# IP-Adress PARSER
##########################################################################
class IpAddress:

    @staticmethod
    def parse(address: str):
        address = address.strip()
        if '@' in address:
            addr, port = address.split("@")
            port = int(port)
        else:
            addr = address
            port = None

        return str(addr), port

    @staticmethod
    def check(ip_addr: str):

        ip_addr = ip_addr.strip()

        if ipv6_address.match(ip_addr) or ipv4_address.match(ip_addr):
            return True
        else:
            return False

