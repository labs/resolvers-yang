"""
Module for converting data from valid loaded Json(Python Dictionary) to Unbound configuration strings.
"""

from re import sub
from resolvers_yang.regex import ipv4_address, ipv6_address
from resolvers_yang.parser import TrustAnchorRR as TA_parser


def generate(config: dict) -> str:
    """
    :param config: Dictionary loaded from Resolvers-YANG Model valid Json file.
    :type config: dict
    :return: Configuration strings for Unbound separated by newlines.
    :rtype: str
    """
    unbound_conf = ""
    if 'cznic-resolver-common:dns-resolver' in config:
        conf = config['cznic-resolver-common:dns-resolver']
    else:
        conf = config

    unbound_conf += "server:"
    if 'network' in conf:
        if unbound_conf != "":
            unbound_conf += "\n"
        unbound_conf += Network.generate(conf['network'])
    if 'server' in conf:
        if unbound_conf != "":
            unbound_conf += "\n"
        unbound_conf += Server.generate(conf['server'])
    if 'logging' in conf:
        if unbound_conf != "":
            unbound_conf += "\n"
        unbound_conf += Logging.generate(conf['logging'])
    if 'dnssec' in conf:
        if unbound_conf != "":
            unbound_conf += "\n"
        unbound_conf += Dnssec.generate(conf['dnssec'])
    if 'cache' in conf:
        if unbound_conf != "":
            unbound_conf += "\n"
        unbound_conf += Cache.generate(conf['cache'])
    if 'debugging' in conf:
        if unbound_conf != "":
            unbound_conf += "\n"
        unbound_conf += Debugging.generate(conf['debugging'])
    if 'dns64' in conf:
        if unbound_conf != "":
            unbound_conf += "\n"
        unbound_conf += Dns64.generate(conf['dns64'])
    if 'resolver' in conf:
        if unbound_conf != "":
            unbound_conf += "\n"
        unbound_conf += Resolver.generate(conf['resolver'])

    return unbound_conf


class Server:

    @staticmethod
    def generate(server: dict) -> str:
        server_conf = ""
        server_conf += Server.username(server['user-name'])

        return server_conf

    @staticmethod
    def username(username: str) -> str:
        conf_string = "\tusername: \"{}\"".format(username)

        return conf_string


class Network:

    @staticmethod
    def generate(network: dict) -> str:
        network_conf = ""
        if 'listen-interfaces' in network:
            if network_conf != "":
                network_conf += "\n"
            network_conf += Network.listen_interfaces(network['listen-interfaces'])
        if 'source-address' in network:
            if network_conf != "":
                network_conf += "\n"
            network_conf += Network.source_address(network['source-address'])
        if 'recursion-transport' in network:
            if network_conf != "":
                network_conf += "\n"
            network_conf += Network.recursion_transport(network['recursion-transport'])
        if 'udp-payload-size' in network:
            if network_conf != "":
                network_conf += "\n"
            network_conf += Network.udp_payload_size(network['udp-payload-size'])

        return network_conf

    @staticmethod
    def listen_interfaces(interfaces: list) -> str:
        conf_string = ""

        for interface in interfaces:
            temp = ""
            # name
            if 'name' in interface:
                pass
            # ip-address
            if 'ip-address' in interface:
                temp = str(interface["ip-address"])
            # port
            if 'port' in interface:
                temp += "@" + str(interface["port"])

            if conf_string != "":
                conf_string += "\n"
            conf_string += "\tinterface: {}".format(temp)

        return conf_string

    @staticmethod
    def source_address(source_address: dict) -> str:
        conf_string = ""

        if 'ipv4' in source_address:
            conf_string += str("\toutgoing-interface: {0}".format(source_address['ipv4']))

        if 'ipv6' in source_address:
            if conf_string != "":
                conf_string += "\n"
            conf_string += str("\toutgoing-interface: {0}".format(source_address['ipv6']))

        return conf_string

    @staticmethod
    def client_transport() -> str:
        pass

    @staticmethod
    def recursion_transport(recursion_transport: dict) -> str:
        """Converting 'recursion-transport' data model option to Unbound configuration string.

                Conversion to Unbound:
                    'ipv4 ipv6'   ->   'do-ip6: yes'
                                       'do-ip4: yes'
                    'ipv4'        ->   'do-ip6: no'
                                       'do-ip4: yes'
                    'ipv6'        ->   'do-ip6: yes'
                                       'do-ip4: no'
                """
        conf_string = ""

        if 'l2-protocols' in recursion_transport:
            protocols = str(recursion_transport['l2-protocols'])

            if 'ipv4' in protocols and 'ipv6' in protocols:
                conf_string += str("\tdo-ip6: yes\n\tdo-ip4: yes")
            elif 'ipv4' in protocols:
                conf_string += str("\tdo-ip6: no\n\tdo-ip4: yes")
            elif 'ipv6' in protocols:
                conf_string += str("\tdo-ip6: yes\n\tdo-ip4: no")
            else:
                conf_string += str("\tdo-ip6: no\n\tdo-ip4: no")
                # raise Exception?

        return conf_string

    @staticmethod
    def udp_payload_size(payload_size: int):
        conf_string = "\tedns-buffer-size: {0}".format(payload_size)
        return conf_string


class Resolver:

    @staticmethod
    def generate(resolver: dict) -> str:
        resolver_conf = ""

        # hints
        if 'hints' in resolver:
            hints = resolver['hints']
            if 'root-hint' in hints:
                if resolver_conf != "":
                    resolver_conf += "\n"
                resolver_conf += Resolver.hints_root_hint(hints['root-hint'])
            if 'root-zone-file' in hints:
                if resolver_conf != "":
                    resolver_conf += "\n"
                resolver_conf += Resolver.hints_root_zone_file(hints['root-zone-file'])
        # options
        if 'options' in resolver:
            options = resolver['options']
            if 'glue-checking' in options:
                if resolver_conf != "":
                    resolver_conf += "\n"
                resolver_conf += Resolver.option_glue_checking(options['glue-checking'])
            if 'qname-minimisation' in options:
                if resolver_conf != "":
                    resolver_conf += "\n"
                resolver_conf += Resolver.option_qname_minimisation(options['qname-minimisation'])
            if 'reorder-rrset' in options:
                if resolver_conf != "":
                    resolver_conf += "\n"
                resolver_conf += Resolver.option_reorder_rrset(options['reorder-rrset'])
            if 'query-loopback' in options:
                if resolver_conf != "":
                    resolver_conf += "\n"
                resolver_conf += Resolver.option_query_loopback(options['query-loopback'])

            if 'stub-zones' in resolver:
                if resolver_conf != "":
                    resolver_conf += "\n"
                resolver_conf += Resolver.stub_zones(resolver['stub-zones'])

        return resolver_conf

    @staticmethod
    def stub_zones(zones: list) -> str:
        conf_string = ""
        for zone in zones:

            zone_conf = "\nstub-zone:\n"
            nameserver = ""

            if 'domain' in zone:
                zone_conf += str("\tname: \"{}\"\n".format(zone["domain"].value))

            if 'nameserver' in zone:
                nameserver = str(zone["nameserver"])
                if 'port' in zone:
                    nameserver += "@" + str(zone["port"])

            if ipv4_address.match(zone["nameserver"].value) or ipv6_address.match(zone["nameserver"].value):
                zone_conf += "\tstub-addr: \"{}\"\n".format(nameserver)
            else:
                zone_conf += "\tstub-host: \"{}\"\n".format(nameserver)

            conf_string += zone_conf

        return conf_string

    @staticmethod
    def hints_root_hint(root_hints: list) -> str:
        conf_string = ""
        # TODO: not DONE
        return conf_string

    @staticmethod
    def hints_root_zone_file(rzf_path: str) -> str:
        conf_string = "\troot-hints: \"{0}\"".format(rzf_path)

        return conf_string

    @staticmethod
    def option_glue_checking(checking: bool) -> str:
        """Converting 'glue-checking' data model option to Unbound configuration string.

        :param checking: Glue Checking on/off.
        :type checking: bool
        :returns:  str
        :raises:

        Conversion to Unbound:
            true    ->  'harden-glue: yes'
            false   ->  'harden-glue: no'
        """

        if bool(checking) is True:
            conf_string = "\tharden-glue: yes"
        elif bool(checking) is False:
            conf_string = "\tharden-glue: no"
        else:
            conf_string = ""

        return conf_string

    @staticmethod
    def option_qname_minimisation(minimisation: bool) -> str:
        """Converting resolvers-yang data model 'qname-minimisaton' option to Unbound configuration string.

        :param minimisation: Qname Minimisation on/off.
        :type minimisation: bool
        :returns:  str
        :raises:

        Conversion to Unbound:
            true    ->  'qname-minimisation: yes'
            false   ->  'qname-minimisation: no'
        """

        if bool(minimisation) is True:
            conf_string = "\tqname-minimisation: yes"
        elif bool(minimisation) is False:
            conf_string = "\tqname-minimisation: no"
        else:
            conf_string = ""

        return conf_string

    @staticmethod
    def option_query_loopback(q_loopback: bool) -> str:
        """Converting resolvers-yang data model 'query-loopback' option to Unbound configuration string.

        :param q_loopback: Query localhost on/off.
        :type q_loopback: bool
        :returns:  str
        :raises:

        Conversion to Unbound:
            true    ->  'do-not-query-localhost: no'
            false   ->  'do-not-query-localhost: yes'
        """

        if bool(q_loopback) is True:
            conf_string = "\tdo-not-query-localhost: no"
        elif bool(q_loopback) is False:
            conf_string = "\tdo-not-query-localhost: yes"
        else:
            conf_string = ""
            # raise Error?

        return conf_string

    @staticmethod
    def option_reorder_rrset(reorder: bool) -> str:
        """Converting resolvers-yang data model 'reorder-rrset' option to Unbound configuration string.

        :param reorder: Reorder rrset.
        :type reorder: bool
        :returns:  str
        :raises:

        Conversion to Unbound:
            true    ->  'rrset-roundrobin: yes'
            false   ->  'rrset-roundrobin: no'
        """

        if bool(reorder) is True:
            conf_string = "\trrset-roundrobin: yes"
        elif bool(reorder) is False:
            conf_string = "\trrset-roundrobin: no"
        else:
            conf_string = ""

        return conf_string


class Logging:

    @staticmethod
    def generate(logging: dict) -> str:
        logging_conf = ""
        if 'verbosity' in logging:
            if logging_conf != "":
                logging_conf += "\n"
            logging_conf += Logging.verbosity(logging['verbosity'])

        return logging_conf

    @staticmethod
    def verbosity(verbosity: int):
        """Converting resolvers-yang model 'verbosity' to Unbound configuration string.

        :param verbosity: Verbosity level, range 0-5.
        :type verbosity: int
        :returns:  str
        :raises:

        Conversion to Unbound:
            0-5   ->  'verbosity: 0-5'
        """
        conf_string = "\tverbosity: {0}".format(verbosity)

        return conf_string


class Dnssec:

    @staticmethod
    def generate(dnssec: dict) -> str:
        dnssec_conf = ""
        if 'trust-anchors' in dnssec:
            if dnssec_conf != "":
                dnssec_conf += "\n"
            dnssec_conf += Dnssec.trust_anchors(dnssec['trust-anchors'])
        if 'negative-trust-anchors' in dnssec:
            if dnssec_conf != "":
                dnssec_conf += "\n"
            dnssec_conf += Dnssec.negative_trust_anchors(dnssec['negative-trust-anchors'])

        return dnssec_conf

    # TODO: not DONE
    @staticmethod
    def trust_anchors(trust_anchors: list):
        conf_string = ""
        # TODO: not sure if it is right
        for trust_anchor in trust_anchors:

            if 'trust-anchor' in trust_anchor:
                for ta in trust_anchor['trust-anchor']:
                    if 'ds' in ta:
                        ta_string = TA_parser.create_ds_string(trust_anchor['domain'],
                                                               ta['ds'])
                    elif 'dnskey' in ta:
                        ta_string = TA_parser.create_dnskey_string(trust_anchor['domain'],
                                                                   ta['dnskey'])
                    else:
                        break

                    if conf_string != "":
                        conf_string += "\n"

                    conf_string += str("\ttrust-anchor: \"{0}\"".format(ta_string))

        return conf_string

    @staticmethod
    def negative_trust_anchors(negative_tas: list):
        conf_string = ""

        for nta in negative_tas:
            if conf_string != "":
                conf_string += "\n"
            conf_string += str("\tdomain-insecure: \"{0}\"".format(nta))
        return conf_string


class Cache:

    @staticmethod
    def generate(cache: dict) -> str:
        cache_conf = ""
        if 'max-size' in cache:
            if cache_conf != "":
                cache_conf += "\n"
            cache_conf += Cache.max_size(cache['max-size'])
        if 'max-ttl' in cache:
            if cache_conf != "":
                cache_conf += "\n"
            cache_conf += Cache.max_ttl(cache['max-ttl'])
        if 'min-ttl' in cache:
            if cache_conf != "":
                cache_conf += "\n"
            cache_conf += Cache.min_ttl(cache['min-ttl'])

        return cache_conf

    @staticmethod
    def current_size():
        conf_string = "cache.current_size"

        return conf_string

    @staticmethod
    def max_size(size: int) -> str:
        conf_string = "\tmsg-cache-size: {0}".format(size)

        return conf_string

    @staticmethod
    def max_ttl(max_ttl: int) -> str:
        conf_string = "\tcache-max-ttl: {0}".format(max_ttl)

        return conf_string

    @staticmethod
    def min_ttl(min_ttl: int) -> str:
        conf_string = "\tcache-min-ttl: {}".format(min_ttl)

        return conf_string


class Debugging:

    @staticmethod
    def generate(debugging: dict) -> str:
        debugging_conf = ""

        if 'cznic-resolver-unbound:val-override-date' in debugging:
            debugging_conf += Debugging.val_override_date(debugging['cznic-resolver-unbound:val-override-date'])

        return debugging_conf

    @staticmethod
    def val_override_date(rrsig_date: str) -> str:

        rrsig_date = sub("-|:|Z|T", "", str(rrsig_date))

        conf_string = str("\tval-override-date: \"{0}\"".format(rrsig_date))

        return conf_string


class Dns64:

    @staticmethod
    def generate(dns64: dict):
        dns64_conf = ""
        if 'prefix' in dns64:
            dns64_conf += Dns64.prefix(dns64['prefix'])

        return dns64_conf

    @staticmethod
    def prefix(prefix: str) -> str:
        conf_string = str("\tdns64-prefix: {0}".format(prefix))

        return conf_string
