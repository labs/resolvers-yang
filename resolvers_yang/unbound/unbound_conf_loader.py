"""
Module for load Unbound configuration string to python dictionary, which can be validate against
'resolvers-yang' data model and save as Json-Encoded file.
"""

from resolvers_yang.parser import TrustAnchorRR, IpAddress
from resolvers_yang.regex import domain_name, ipv4_address, ipv6_address, ipv6_with_prefix


def load(unbound_conf: str) -> dict:
    """
    :param unbound_conf: data loaded from Unbound configuration file
    :type unbound_conf: str
    :return: Dictionary structure of common-resolver configuration
    :rtype: dict
    """

    loader = _Loader()
    dict_conf = loader.make_dictionary(unbound_conf)

    return dict_conf


class _Loader:

    def __init__(self):
        self._reset()

    _server_conf_switcher = {
        "username": "_username",
        "root-hints": "_roothints",
        "interface": "_interface",
        "outgoing-interface": "_out_interface",
        "do-ip4": "_do_ipv4",
        "do-ip6": "_do_ipv6",
        "edns-buffer-size": "_edns_buffer_size",
        "harden-glue": "_harden_glue",
        "qname-minimisation": "_qname_minimisation",
        "rrset-roundrobin": "_rrset_roundrobin",
        "do-not-query-localhost": "_dnq_localhost",
        "verbosity": "_verbosity",
        "trust-anchor": "_trust_anchor",
        "domain-insecure": "_domain_insecure",
        "msg-cache-size": "_cache_size",
        "cache-max-ttl": "_cache_maxttl",
        "cache-min-ttl": "_cache_minttl",
        "val-override-date": "_date_override",
        "dns64-prefix": "_dns64_prefix",
    }

    _stubzone_conf_switcher = {
        "name": "_stubname",
        "stub-addr": "_stubaddr",
        "stub-host": "_stubhost",
    }

    def make_dictionary(self, conf_string: str) -> dict:

        self._parse_string(conf_string)

        config = {}
        if bool(self.server):
            config['server'] = self.server
        if bool(self.network):
            self._recursion_transport()
            config['network'] = self.network
        if bool(self.resolver):
            config['resolver'] = self.resolver
        if bool(self.logging):
            config['logging'] = self.logging
        if bool(self.dnssec):
            config['dnssec'] = self.dnssec
        if bool(self.cache):
            config['cache'] = self.cache
        if bool(self.debugging):
            config['debugging'] = self.debugging
        if bool(self.dns64):
            config['dns64'] = self.dns64

        return {"cznic-resolver-common:dns-resolver": config}

    def _reset(self):
        self.server = {}
        self.network = {}
        self.resolver = {}
        self.logging = {}
        self.dnssec = {}
        self.cache = {}
        self.debugging = {}
        self.dns64 = {}

        self.network = {'listen-interfaces': []}
        self.resolver = {'stub-zones': [], 'options': {}}
        self.dnssec = {'trust-anchors': [], 'negative-trust-anchors': []}

        self.stub_zones = {}

        self.do_ipv4 = True
        self.do_ipv6 = True

        self.interface_count = 0
        self.ta_domain_count = 0

    def _parse_string(self, unbound_conf: str):

        lines_list = unbound_conf.splitlines()

        server_conf = False
        stubzone_conf = False

        for line in lines_list:

            # parse key and value from line
            key, value = _Loader._parse_line(line)

            if key == 'server':
                server_conf = True
                stubzone_conf = False
            elif key == 'stub-zone':
                stubzone_conf = True
                server_conf = False
            # ending for unbound.rpl files
            elif key == 'CONFIG_END':
                break
            else:
                if server_conf:
                    mname = _Loader._server_conf_switcher.get(key, "_noop")
                    method = getattr(self, mname)
                    method(key, value)

                elif stubzone_conf:
                    mname = _Loader._stubzone_conf_switcher.get(key, "_noop")
                    method = getattr(self, mname)

                    method(key, value)

    @staticmethod
    def _parse_line(line: str):
        line = line.strip()

        # remove comments ofter # or ;
        line = _Loader._ignore_comments(line)

        # separation of key and value
        (key, separator, value) = line.partition(":")

        return key.strip(), value.strip(' "')

    @staticmethod
    def _ignore_comments(line: str):

        if "#" in line and ";" not in line:
            nocomment_line, comment = line.split("#", 1)
        elif "#" not in line and ";" in line:
            nocomment_line, comment = line.split(";", 1)
        elif "#" in line and ";" in line:
            if line.find("#") < line.find(";"):
                nocomment_line, comment = line.split("#", 1)
            else:
                nocomment_line, comment = line.split(";", 1)
        else:
            nocomment_line = line

        return nocomment_line.strip()

    ############################################################
    # SERVER node
    ############################################################
    def _username(self, key: str, value: str) -> None:
        self.server['user-name'] = str(value)

    def _roothints(self, key: str, value: str) -> None:
        self.resolver['hints'] = {'root-zone-file': str(value)}

    def _interface(self, key: str, value: str) -> None:
        name = "interface" + str(self.interface_count)
        address, port = IpAddress.parse(value)
        self.interface_count += 1

        if port:
            self.network['listen-interfaces'].append({
                'name': str(name),
                'ip-address': str(address),
                'port': int(port)
            })
        else:
            self.network['listen-interfaces'].append({
                'name': str(name),
                'ip-address': str(address)
            })

    def _out_interface(self, key: str, value: str) -> None:

        if ipv4_address.match(value):
            self.network['source-address'] = {'ipv4': value}
        elif ipv6_address.match(value):
            self.network['source-address'] = {'ipv6': value}

    def _do_ipv4(self, key: str, value: str) -> None:

        if value == 'no':
            self.do_ipv4 = False
        elif value == 'yes':
            self.do_ipv4 = True

    def _do_ipv6(self, key: str, value: str) -> None:
        if value == 'no':
            self.do_ipv6 = False
        elif value == 'yes':
            self.do_ipv6 = True

    def _edns_buffer_size(self, key: str, value: str) -> None:
        self.network['udp-payload-size'] = int(value)

    def _harden_glue(self, key: str, value: str) -> None:
        if value == 'yes':
            self.resolver['options']['glue-checking'] = True
        elif value == 'no':
            self.resolver['options']['glue-checking'] = False

    def _qname_minimisation(self, key: str, value: str) -> None:
        if value == 'yes':
            self.resolver['options']['qname-minimisation'] = True
        elif value == 'no':
            self.resolver['options']['qname-minimisation'] = False

    def _rrset_roundrobin(self, key: str, value: str) -> None:
        if value == 'yes':
            self.resolver['options']['reorder-rrset'] = True
        elif value == 'no':
            self.resolver['options']['reorder-rrset'] = False

    def _dnq_localhost(self, key: str, value: str) -> None:
        if value == 'yes':
            self.resolver['options']['query-loopback'] = False
        elif value == 'no':
            self.resolver['options']['query-loopback'] = True

    def _verbosity(self, key: str, value: str) -> None:
        self.logging['verbosity'] = int(value)

    def _trust_anchor(self, key: str, value: str) -> None:

        if 'DS' in value:
            ta = TrustAnchorRR.parse_ds(value)
        elif 'DNSKEY' in value:
            ta = TrustAnchorRR.parse_dnskey(value)
        else:
            return

        domain = ta['owner']
        if not any(item['domain'] == domain for item in self.dnssec['trust-anchors']):
            self.dnssec['trust-anchors'].append({"domain": domain, "auto-update": True, "trust-anchor": []})

        ta_index = 0
        for tas in self.dnssec['trust-anchors']:
            if tas['domain'] == domain:
                break
            else:
                ta_index += 1

        ta['id'] = len(self.dnssec['trust-anchors'][ta_index]['trust-anchor'])

        del ta['owner']

        self.dnssec['trust-anchors'][ta_index]['trust-anchor'].append(ta)

    def _domain_insecure(self, key: str, value: str) -> None:
        if domain_name.match(value):
            self.dnssec['negative-trust-anchors'].append(str(value))

    def _cache_size(self, key: str, value: str) -> None:
        self.cache['max-size'] = int(value)

    def _cache_minttl(self, key: str, value: str) -> None:
        self.cache['min-ttl'] = int(value)

    def _cache_maxttl(self, key: str, value: str) -> None:
        self.cache['max-ttl'] = int(value)

    def _date_override(self, key: str, value: str) -> None:

        utc_datetime = value[:4] + '-' + value[4:6] + '-' \
                       + value[6:8] + 'T' + value[8:10] \
                       + ':' + value[10:12] + ':' + value[12:] + 'Z'

        self.debugging['cznic-resolver-unbound:val-override-date'] = utc_datetime

    def _dns64_prefix(self, key: str, value: str) -> None:
        if ipv6_with_prefix.match(value):
            self.dns64['prefix'] = str(value)

    # STUB-ZONE
    def _stubname(self, key: str, value: str) -> None:
        self.stub_zones = {}
        if domain_name.match(value):
            self.stub_zones['domain'] = value

    def _stubaddr(self, key: str, value: str) -> None:

        self.stub_zones['nameserver'], self.stub_zones['port'] = IpAddress.parse(value)

        if self.stub_zones['domain'] and self.stub_zones['port']:

            self.resolver['stub-zones'].append({
                "domain": self.stub_zones['domain'],
                "nameserver": self.stub_zones['nameserver'],
                "port": int(self.stub_zones['port'])
            })
        elif self.stub_zones['nameserver'] and self.stub['domain']:
            self.resolver['stub-zones'].append({
                "domain": self.stub_zones['domain'],
                "nameserver": self.stub_zones['nameserver']
            })

    def _stubhost(self, key: str, value: str) -> None:

        self.stub_zones['nameserver'], self.stub_zones['port'] = IpAddress.parse(value)

        if self.stub_zones['domain'] and self.stub_zones['port']:

            self.resolver['stub-zones'].append({
                "domain": self.stub_zones['domain'],
                "nameserver": self.stub_zones['nameserver'],
                "port": self.stub_zones['port']
            })
        elif self.stub_zones['domain']:
            self.resolver['stub-zones'].append({
                "domain": self.stub_zones['domain'],
                "nameserver": self.stub_zones['nameserver']
            })

    ##########################################################
    # default function
    def _noop(self, key: str, value: str) -> None:

        if not value.strip() == "" or not key.strip() == "":
            print("Unknown Key: {0} with Value: {1}".format(value.strip(), key.strip()))

    def _recursion_transport(self):
        l2_protocols = 'ipv4 ipv6'
        if self.do_ipv4 and not self.do_ipv6:
            l2_protocols = 'ipv4'
        elif self.do_ipv6 and not self.do_ipv4:
            l2_protocols = 'ipv6'
        self.network['recursion-transport'] = {'l2-protocols': l2_protocols}



