import os
import codecs
from setuptools import setup, find_packages

here = os.path.abspath(os.path.dirname(__file__))
with codecs.open(os.path.join(here, 'README.md'), encoding='utf-8') as readme:
    long_description = readme.read()

setup(
    name='resolvers_yang',
    packages=find_packages(),
    include_package_data=True,
    use_scm_version=True,
    setup_requires=["setuptools_scm"],
    description='YANG data models and tools for unified configuration of DNS resolvers',
    long_description=long_description,
    url="https://gitlab.labs.nic.cz/labs/resolvers-yang",
    author=["Ladislav Lhotka",
            "Ales Mrazek"],
    author_email=["lhotka@nic.cz",
                  "ales.mrazek@nic.cz"],
    install_requires=["yangson"],
    keywords=["yang", "data model", "Knot Resolver", "Unbound", "configuration", "generator"],
    classifiers=[
        "Programming Language :: Python :: 3.5",
        "Development Status :: 3 - Alpha",
        "Intended Audience :: System Administrators",
        "Intended Audience :: Telecommunications Industry",
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
        "Operating System :: OS Independent",
        "Topic :: System :: Monitoring",
        "Topic :: System :: Systems Administration"
    ],
)




