"""
This test is generating unbound.conf file, which is converted back to JSON
and again unbound-test.conf is generated using this JSON.
Then unbound.conf and unbound-test.conf are compared.
"""

from json import load, dump
from yangson.datamodel import DataModel

from resolvers_yang.unbound import unbound_conf_gen
from resolvers_yang.unbound import unbound_conf_loader

examample_path = "../examples/example-data.json"
test_path = "test-data.json"

# path to directory where are yang modules
yangdir = "../yang-modules"

# names of configuration files and paths where it will be created
unb_path = "unbound.conf"
unbtest_path = "unbound-test.conf"

# yangson load DataModel
model = DataModel.from_file(yangdir + "/yanglib.json", [yangdir])


##################################################################################
# Generating unbound-test.conf

# load data from Json-encoded file
with open(examample_path) as infile:
    ri = load(infile)

# load data to DataModel
data = model.from_raw(ri)

# validate against DataModel
data.validate()

# adding missing default values
data_defaults = data.add_defaults()

# generate configuration strings
unb_conf = unbound_conf_gen.generate(data_defaults)

# write Unbound configurationwith
with open(unb_path, "w") as unb_file:
    unb_file.write(unb_conf)

#################################################################################
# Converting unbound-test.conf

# load unbound.conf file
with open(unb_path, "r") as unb_file:
    unbconf_data = unb_file.read()

# call of conversion function     unbconf_data >> Dictionary
json_data = unbound_conf_loader.load(unbconf_data)

# load data to DataModel
model_data = model.from_raw(json_data)

# validate data against DataModel
model_data.validate()

# save model_data to json
with open(test_path, 'w') as json_file:
    dump(json_data, json_file, indent=2, sort_keys=False)

#################################################################################
# Generate unbound-test.conf

# load data from Json-encoded file
with open(test_path) as infile:
    test = load(infile)

# load data to DataModel
data = model.from_raw(test)

# validate against DataModel
data.validate()

# adding missing default values
data_defaults = data.add_defaults()

# generate configuration strings
unbtest_conf = unbound_conf_gen.generate(data_defaults)

# write Unbound configuration
unbtest_file = open(unbtest_path, "w+")
unbtest_file.write(unbtest_conf)
unbtest_file.close()

#################################################################################
# Comparing unbound.conf and unbound-test.conf

# load unbound.conf file
with open(unb_path, "r") as unb_file:
    unbconf_data = unb_file.readlines()

# load unbound-test.conf file
with open(unb_path, "r") as unb_file:
    unbconftest_data = unb_file.readlines()

i = 0
for a, b in zip(unbconf_data, unbconftest_data):
    i += 1
    if a != b:
        print("Line: " + str(i) + ":" + b + " is not same as " + a)
